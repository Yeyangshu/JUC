/**
 * Copyright (C), 2018-2020
 * FileName: Constants
 * Author:   11077
 * Date:     2020/6/22 23:47
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yeyangshu.juc.juc008.c01_HashTableToConcurrentHashMap;

/**
 * @author yeyangshu
 * @version 1.0
 * @date 2020/6/22 23:47
 */
public class Constants {
    public static final int COUNT = 1000000;
    public static final int THREAD_COUNT = 100;
}
